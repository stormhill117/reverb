#ifndef REVERB_H
#define REVERB_H 1

#include <stdint.h>

/* Pass in the next sample here.
 * Returns the next result. */
int16_t next_sample(int16_t);

#endif /* REVERB_H */
