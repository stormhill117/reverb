#include "reverb.h"

#include <stddef.h>

/* How far apart our taps should be taken. */
#define TAP_STEP 48
/* How many taps to take. */
#define TAP_COUNT 4

#define BUF_SIZ TAP_COUNT*TAP_STEP

static int16_t samples[BUF_SIZ] = {};

/* How much each tap gets attenuated. */
static const int16_t attenuation = 0x4000;

static size_t head = 0;

int16_t next_sample(const int16_t sample){
	/* Our accumulators. */
	int32_t accum = sample;
	int16_t atten_accum = 0x7FFF;

	/* Add in attenuated samples. */
	for(size_t tap = 0; tap < TAP_COUNT; tap++){
		/* Adjust the attenuation sample. */
		atten_accum = (int16_t)(((int32_t)atten_accum*attenuation) >> 16);

		/* Calculate the tap index using modular arithmetic. */
		const size_t tap_idx_dist = tap*TAP_STEP;
		size_t tap_idx = 0;
		if(head >= tap_idx_dist){
			tap_idx = head - tap_idx_dist;
		}else{
			tap_idx = BUF_SIZ - (tap_idx_dist - head);
		}

		accum += ((int32_t)atten_accum*samples[tap_idx]) >> 16;
	}

	/* Now store the sample and advance head. */
	samples[head] = sample;
	if(head < BUF_SIZ-1){
		head++;
	}else{
		head = 0;
	}

	/* Now saturate the sample.
 	 * Why not earlier? What if we had large swings back and forth,
 	 * after we would be saturated? We'd lose more accuracy. */

	int16_t res = 0;

	if(accum > INT16_MAX){
		res = INT16_MAX;
	}else if(accum < INT16_MIN){
		res = INT16_MIN;
	}else{
		res = (int16_t)accum;
	}

	return res;
}
