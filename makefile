CC=gcc
CFLAGS=-O2 -I. -std=c99 -Wall -Wextra
LDFLAGS=

reverb: obj/reverb.o obj/main.o
	$(CC) -o $@ $^ $(LDFLAGS)

obj/reverb.o: reverb.c reverb.h
	$(CC) -c -o $@ $< $(CFLAGS)

obj/main.o: main.c reverb.h
	$(CC) -c -o $@ $< $(CFLAGS)

.PHONY: clean

clean:
	rm -rf obj/ reverb
