#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "reverb.h"

int main(int argc, char **argv){
	if(argc < 2){
		printf("Usage: %s (<in>|-) [<out>]\n", argv[0]);
		return EXIT_SUCCESS;
	}

	/* Get files. */
	FILE * const infile = strcmp(argv[1], "-")?
		fopen(argv[1], "rb"):
		stdin;
	if(!infile){
		perror("fopen() input failed");
		return EXIT_FAILURE;
	}
	FILE * const outfile = (argc > 2)?
		fopen(argv[2], "wb"):
		stdout;
	if(!outfile){
		perror("fopen() output failed");
		return EXIT_FAILURE;
	}

	while(1){
		/* Get a sample. */
		int16_t in = 0;
		fread(&in, sizeof(int16_t), 1, infile);
		/* Check if we're done or an error happened. */
		if(feof(infile)){
			/* Done! */
			break;
		}else if(ferror(infile)){
			perror("fread() error");
			return EXIT_FAILURE;
		}

		/* Process the sample. */
		const int16_t out = next_sample(in);

		/* Write it out. */
		if(fwrite(&out, sizeof(int16_t), 1, outfile) < 1){
			perror("fwrite() error");
			return EXIT_FAILURE;
		}
	}

	/* Cleanup time! */
	if(infile != stdin){
		/* Actually, this should be safe anyways. */
		if(fclose(infile)){
			perror("fclose() input failed");
			return EXIT_FAILURE;
		}
	}
	if(outfile != stdout){
		if(fclose(outfile)){
			perror("fclose() output failed");
			return EXIT_FAILURE;
		}
	}

	return EXIT_SUCCESS;
}
